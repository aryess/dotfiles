let data_dir = has('nvim') ? stdpath('data') . '/site' : '~/.vim'
if empty(glob(data_dir . '/autoload/plug.vim'))
  silent execute '!curl -fLo '.data_dir.'/autoload/plug.vim --create-dirs  https://raw.githubusercontent.com/junegunn/vim-plug/master/plug.vim'
  autocmd VimEnter * PlugInstall --sync | source $MYVIMRC
endif

call plug#begin('~/.vim/plugged')

Plug 'https://github.com/airblade/vim-gitgutter.git' " Git markers in guter
  Plug 'https://github.com/leafgarland/typescript-vim' " Typescript support
Plug 'https://github.com/mattn/emmet-vim' " Emmet for vim
"  Plug 'https://github.com/solarnz/thrift.vim'
Plug 'https://github.com/mg979/vim-visual-multi' " Sublime style multi cursor
"  Plug 'https://github.com/tpope/vim-abolish' " Search , substitute, and abbreviate variants of a word
  Plug 'https://github.com/tpope/vim-fugitive' " Vim aware git integration
  Plug 'https://github.com/tpope/vim-surround' " Delete/change/add parentheses/quotes/XML-tags
Plug 'nanotech/jellybeans.vim' " Theme
Plug 'itchyny/lightline.vim' " Better bottom line
let g:lightline = {
      \ 'colorscheme': 'jellybeans',
      \ }
Plug 'junegunn/fzf', { 'dir': '~/.fzf', 'do': './install --all' }
Plug 'junegunn/fzf.vim'
Plug 'pangloss/vim-javascript' " JS support
Plug 'preservim/nerdcommenter' " Easier commenting and uncommenting.
"  Plug 'prettier/vim-prettier', {
"      \ 'do': 'yarn install',
"      \ 'for': ['javascript', 'typescript', 'css', 'less', 'scss', 'json', 'graphql'] }

Plug 'https://github.com/junegunn/goyo.vim' " Distraction free mode
Plug 'https://github.com/vimlab/split-term.vim' " Utilites around neovim's `:terminal`.
Plug 'https://github.com/w0rp/ale' " Async 'real time' linting
"  Plug 'https://github.com/rking/ag.vim' " `ag` in vim. deprecated
Plug 'https://github.com/jiangmiao/auto-pairs' " Insert or delete brackets, parens, quotes in pair.
" Plug 'https://github.com/Valloric/YouCompleteMe' " autocomplete
Plug 'https://github.com/leafgarland/typescript-vim' " TS support
" Plug 'https://github.com/Quramy/vim-js-pretty-template' " Syntax color for JS Template Strings
Plug 'https://github.com/alvan/vim-closetag' " Automatically close tags
" Plug 'https://github.com/Quramy/tsuquyomi' " TS language server integration
call plug#end()


" Window navigation
" nmap <silent> <A-Up> :wincmd k<CR>
" nmap <silent> <A-Down> :wincmd j<CR>
" nmap <silent> <A-Left> :wincmd h<CR>
" nmap <silent> <A-Right> :wincmd l<CR>

" sensible defaults
set autoread "relaod if disk change and no unsaved changes
set backspace=indent,eol,start
set background=dark
set colorcolumn=81,120 "rulers
set expandtab "spaces for tabs
set laststatus=2 "always show status line
set noshowmode " Let lightline display status line
set relativenumber "set relative line numbers
set number "set number on current line (not zero)
set scrolloff=15 " show a few lines after bottom
set shiftwidth=4 " change the number of spaces inserted for indentation
set showmatch "show matching braces
set smartindent " logical indents in insert
set t_Co=256 " 256 bit colors
set tabstop=4 " 4 space tabs

syntax enable " syntax highlightings
highlight ColorColumn ctermbg=6

" use system clipbaord
set clipboard=unnamed
set wrapscan

" fzf bindings
nnoremap <Leader>b :Buffers<CR>
nnoremap <Leader>- :Explore<CR>
map <space> :BLines<CR>
nnoremap <CR> :Files<CR>
" dont fuck up other places you need enter
augroup commandlinewindow
  autocmd!
  autocmd CmdwinEnter * nnoremap <buffer> <CR> <CR>
augroup END
autocmd BufReadPost quickfix nnoremap <buffer> <CR> <CR>

" Fzf fill screen
let g:fzf_layout = { 'down': '~50%' }

" Switch to alternate file with tab
nnoremap <Tab> <C-^><CR>
" :nnoremap <Tab> :bnext<CR>
" :nnoremap <S-Tab> :bprevious<CR>
" set hidden

" comment toggle - include leading space
let g:NERDSpaceDelims = 1
nnoremap <silent> <leader>cc :call NERDComment(0,"toggle")<CR>

" Remove trailing whitespace on save
autocmd BufWritePre * %s/\s\+$//e

" set css indents to 2
autocmd Filetype css setlocal ts=2 sw=2 sts=0 expandtab
let $NVIM_TUI_ENABLE_TRUE_COLOR=1
set termguicolors



" refresh view F5
nmap <silent> 8 :e!<CR>
" Ctrl j/k to move in whitespaced chunks
nnoremap <C-j> }
nnoremap <C-k> {

" fold settings
set foldmethod=indent
set foldlevel=200

" Put full path of current buffer on clipboard
" nmap <silent> <Leader>p :let @+ = expand('%:p')<CR>
nmap <silent> <Leader>p :Prettier<CR>

" Format json
nmap <silent> <Leader>j :%!python -m json.tool<CR>
"colorscheme Carbonight
colorscheme jellybeans

" Create new vertical split
nnoremap <leader>v :vnew<CR>
" Create new horizontal split
nnoremap <leader>h :new<CR>
" Close split
nnoremap <leader>q :close<CR>

" Goyo
nnoremap <leader>] :Goyo<CR>
let g:goyo_height = '100%'
let g:goyo_width = 120

" autocmd! User GoyoEnter Limelight
" autocmd! User GoyoLeave Limelight!

" Close current buffer
nmap <leader>d :bprevious<CR>:bdelete #<CR>

" Open new vertical splits on the right
set splitright

let g:multi_cursor_exit_from_visual_mode=0
nmap s <C-w>

"Ale
let g:ale_fixers = {
\   'javascript': ['eslint'],
\}

" Filepath in status line
set statusline+=%F


" Select current level of indentation
function! SelectIndent ()
  let temp_var=indent(line("."))
  while indent(line(".")-1) >= temp_var
    exe "normal k"
  endwhile
  exe "normal V"
  while indent(line(".")+1) >= temp_var
    exe "normal j"
  endwhile
endfun
nmap <leader>i :call SelectIndent()<CR>

" Sort current level of indentation
nmap <leader>s :call SelectIndent()<CR>:sort<CR>


" auto complete window config
set pumheight=6
let $FZF_DEFAULT_COMMAND = 'ag -g ""'
let g:ycm_autoclose_preview_window_after_insertion = 1
let g:ycm_autoclose_preview_window_after_completion = 1
set completeopt-=preview

" console log macro
let @j="Iconsole.log('xxx', xxx);\=\=f'wve"
let @k="Ixxx.subscribe(v => {console.log('xxx', v);});\=\=fxbve"
let @l="Itap(v => { console.log('xxx', v); }),\=\=fxve"
let @s=":Ag \<cr>"
let @i="I@Input() : xxx;\=\=f:i"
let @o="I@Output() = new EventEmitter<>();\=\=f=i"

" let g:loaded_python_provider=1

if $TERM_PROGRAM =~ "iTerm"
    let &t_SI = "\<Esc>]50;CursorShape=1\x7" " Vertical bar in insert mode
    let &t_EI = "\<Esc>]50;CursorShape=0\x7" " Block in normal mode
endif

"Disable error bells
set noerrorbells visualbell t_vb=
autocmd GUIEnter * set visualbell t_vb=

" tab cycle shortcut
nnoremap <leader>t :tabn<CR>
nnoremap <leader>T :tabe<CR>


" prettier
let g:prettier#config#bracket_spacing = 'true'
let g:prettier#config#single_quote = 'true'

" template highlighting in ts files
" autocmd FileType typescript JsPreTmpl html
" autocmd FileType typescript syn clear foldBraces

" closetag
let g:closetag_filenames = '*.ts,*.html,*.xhtml,*.phtml'

set mouse=a
nmap <leader>[ :so ~/.vimrc<CR>
nmap <leader>o :syntax sync minlines=20<CR>

nnoremap <silent> <leader>c :exec "color " .
    \ ((g:colors_name == "Carbonight") ? "solarized8_light_flat" : "Carbonight")<CR>

