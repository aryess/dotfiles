## Aliases
alias zshconfig="vim ~/.zshrc"
alias la5d="docker-compose exec app php artisan"
alias vim="nvim"
alias ls='exa --icons --grid -h -F'
alias ll='exa -l -h --icons --tree --git --level=2 -F'
alias llt='exa -l -h --icons --tree --git -F'
alias j='jump'
alias gdiff='git difftool --no-symlinks --dir-diff'
alias icat='kitty +kitten icat'
alias kitten='kitty +kitten'
alias ssh="kitty +kitten ssh"

if command -v batcat &> /dev/null
then
    alias bat="batcat"
fi

## Functions
fh() {   eval $( ([ -n "$ZSH_NAME" ] && fc -l 1 || history) | fzf +s --tac | sed 's/ *[0-9]* *//'); }

## Exports
export PATH="${PATH}:${HOME}/.local/bin"
export PATH="${PATH}:${HOME}/.config/composer/vendor/bin"
export GOPATH="$HOME/workspace/go"
export PATH="$HOME/Workspace/tools/arcanist/bin:$PATH"
export PATH="$HOME/.scripts/:$PATH"
export PATH=$PATH:$GOPATH/bin
export NVM_DIR="$HOME/.nvm"
