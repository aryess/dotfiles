#!/bin/sh

#if [[ ! -e $HOME/.nvm ]]; then
#  echo "All done";
#  exit;
#fi

echo "Pre-hook"
read -p "Are you sure? " -n 1 -r
echo    # (optional) move to a new line
if [[ ! $REPLY =~ ^[Yy]$ ]]
then
echo "Nope"
  exit;
fi
echo "Whoops"


### vars
workspaceDir="$HOME/Workspace"
wpDir="$HOME/Pictures/Wallpapers"

### Setup folders
echo "** Creating required folders **\n"
mkdir -pv $workspaceDir -v 2> /dev/null
mkdir -pv $wpDir -v 2> /dev/null
mkdir -pv $HOME/.local/bin -v 2> /dev/null

echo "** Copying default wallpaper **"
# Either find default image, or DL new one
cp /usr/share/backgrounds/f$(rpm -E %fedora)/default/tv-wide/f$(rpm -E %fedora).png $wpDir

# Install everything needed
sudo dnf update -y
sudo dnf copr enable gregw/i3desktop -y
sudo dnf copr enable taw/Riot -y
sudo dnf config-manager --add-repo https://brave-browser-rpm-release.s3.brave.com/x86_64/
sudo rpm --import https://brave-browser-rpm-release.s3.brave.com/brave-core.asc
sudo dnf install https://download1.rpmfusion.org/free/fedora/rpmfusion-free-release-$(rpm -E %fedora).noarch.rpm
sudo dnf install -y curl git vim zsh i3-gaps brave-keyring brave-browser feh i3lock-color i3status w3m-img htop rofi ranger network-manager-applet light tldr neofetch the_silver_searcher stow

### Install non-dnf based things

# Betterlockscreen
curl https://raw.githubusercontent.com/pavanjadhaw/betterlockscreen/master/betterlockscreen -o $HOME/.local/bin/betterlockscreen
chmod u+x $HOME/.local/bin/betterlockscreen

# PyWal
pip3 install --user pywal

# Joplin
wget -O - https://raw.githubusercontent.com/laurent22/joplin/master/Joplin_install_and_update.sh | bash


