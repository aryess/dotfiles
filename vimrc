call plug#begin('~/.vim/plugged')
if has('nvim')
else
  Plug 'roxma/nvim-yarp'
  Plug 'roxma/vim-hug-neovim-rpc'
endif

Plug 'https://github.com/airblade/vim-gitgutter.git'
Plug 'https://github.com/gioele/vim-autoswap'
Plug 'https://github.com/leafgarland/typescript-vim'
Plug 'https://github.com/mattn/emmet-vim'
Plug 'https://github.com/solarnz/thrift.vim'
Plug 'https://github.com/terryma/vim-multiple-cursors'
Plug 'https://github.com/tpope/vim-abolish'
Plug 'https://github.com/tpope/vim-fugitive'
Plug 'https://github.com/tpope/vim-surround'
Plug 'itchyny/lightline.vim'
Plug 'junegunn/fzf', { 'dir': '~/.fzf', 'do': './install --all' }
Plug 'junegunn/fzf.vim'
Plug 'pangloss/vim-javascript'
Plug 'scrooloose/nerdcommenter'
Plug 'prettier/vim-prettier', {
    \ 'do': 'yarn install',
    \ 'for': ['javascript', 'typescript', 'css', 'less', 'scss', 'json', 'graphql'] }
Plug 'https://github.com/junegunn/goyo.vim'
Plug 'https://github.com/junegunn/limelight.vim'
"Plug 'https://github.com/vim-airline/vim-airline'
Plug 'https://github.com/vimlab/split-term.vim'
Plug 'https://github.com/w0rp/ale'
Plug 'https://github.com/rking/ag.vim'
Plug 'https://github.com/danro/rename.vim'
Plug 'https://github.com/jiangmiao/auto-pairs'
Plug 'https://github.com/Valloric/YouCompleteMe'
Plug 'https://github.com/leafgarland/typescript-vim'
" Plug 'https://github.com/Quramy/vim-js-pretty-template'
Plug 'https://github.com/alvan/vim-closetag'
Plug 'https://github.com/tpope/vim-obsession'
Plug 'https://github.com/lifepillar/vim-solarized8'
" Plug 'https://github.com/Quramy/tsuquyomi'
" Plug 'https://github.com/Shougo/vimproc.vim', { 'do': 'make' }
call plug#end()


"Python
" let g:python2_host_prog = '/usr/local/bin/python'
" let g:python3_host_prog = '/usr/local/bin/python3'


" Window navigation
" nmap <silent> <A-Up> :wincmd k<CR>
" nmap <silent> <A-Down> :wincmd j<CR>
" nmap <silent> <A-Left> :wincmd h<CR>
" nmap <silent> <A-Right> :wincmd l<CR>

"use <tab> for completion
" function! TabWrap()
    " if pumvisible()
        " return "\<C-N>"
    " elseif strpart( getline('.'), 0, col('.') - 1 ) =~ '^\s*$'
        " return "\<tab>"
    " elseif &omnifunc !~ ''
        " return "\<C-X>\<C-O>"
    " else
        " return "\<C-N>"
    " endif
" endfunction

" power tab
" imap <silent><expr><tab> TabWrap()

" Escape: exit autocompletion, go to Normal mode
" inoremap <silent><expr> <Esc> pumvisible() ? "<C-e><Esc>" : "<Esc>"





" sensible defaults
set autoread "relaod if disk change and no unsaved changes
set backspace=indent,eol,start
set background=dark
set colorcolumn=81,120 "rulers
set expandtab "spaces for tabs
set laststatus=2 "always show status line
set relativenumber "set relative line numbers
set number "set number on current line (not zero)
set scrolloff=4 " show a few lines after bottom
set shiftwidth=2 " change the number of spaces inserted for indentation
set showmatch "show matching braces
set smartindent " logical indents in insert
set t_Co=256 " 256 bit colors
set tabstop=2 " 2 space tabs
" set php indents to 4
autocmd Filetype php setlocal ts=4 sw=4 sts=0 expandtab
" set thrift indents to 4
autocmd Filetype thrift setlocal ts=4 sw=4 sts=0 expandtab
" set python indents to 4
autocmd Filetype python setlocal ts=4 sw=4 sts=0 expandtab

syntax enable " syntax highlightings
highlight ColorColumn ctermbg=6

" use system clipbaord
set clipboard=unnamed
set wrapscan

" fzf bindings
nmap <silent> 9 :Buffers<CR>
map <space> :BLines<CR>
nnoremap <CR> :Files<CR>
" dont fuck up other places you need enter
augroup commandlinewindow
  autocmd!
  autocmd CmdwinEnter * nnoremap <buffer> <CR> <CR>
augroup END
autocmd BufReadPost quickfix nnoremap <buffer> <CR> <CR>

" Fzf fill screen
let g:fzf_layout = { 'down': '~100%' }

" Switch to alternate file with tab
nnoremap <Tab> <C-^><CR>
" :nnoremap <Tab> :bnext<CR>
" :nnoremap <S-Tab> :bprevious<CR>
" set hidden

" comment toggle - include leading space
let g:NERDSpaceDelims = 1
nnoremap <silent> <leader>cc :call NERDComment(0,"toggle")<CR>

" Remove trailing whitespace on save
autocmd BufWritePre * %s/\s\+$//e

" set css indents to 2
autocmd Filetype css setlocal ts=2 sw=2 sts=0 expandtab
let $NVIM_TUI_ENABLE_TRUE_COLOR=1
set termguicolors



" refresh view F5
nmap <silent> 8 :e!<CR>
" Ctrl j/k to move in whitespaced chunks
nnoremap <C-j> }
nnoremap <C-k> {

" fold settings
set foldmethod=indent
set foldlevel=200

" Put full path of current buffer on clipboard
" nmap <silent> <Leader>p :let @+ = expand('%:p')<CR>
nmap <silent> <Leader>p :Prettier<CR>

" Format json
nmap <silent> <Leader>j :%!python -m json.tool<CR>
" colorscheme Carbonight

" Create new vertical split
nnoremap <leader>v :vnew<CR>
" Create new horizontal split
nnoremap <leader>h :new<CR>
" Close split
nnoremap <leader>q :close<CR>

" Goyo
nnoremap <leader>] :Goyo<CR>
let g:goyo_height = '100%'
let g:goyo_width = 120

" autocmd! User GoyoEnter Limelight
" autocmd! User GoyoLeave Limelight!

" Close current buffer
nmap <leader>d :bprevious<CR>:bdelete #<CR>

" Open new vertical splits on the right
set splitright

let g:multi_cursor_exit_from_visual_mode=0
nmap s <C-w>

"Ale
let g:ale_fixers = {
\   'javascript': ['eslint'],
\}

" Filepath in status line
set statusline+=%F


" Select current level of indentation
function! SelectIndent ()
  let temp_var=indent(line("."))
  while indent(line(".")-1) >= temp_var
    exe "normal k"
  endwhile
  exe "normal V"
  while indent(line(".")+1) >= temp_var
    exe "normal j"
  endwhile
endfun
nmap <leader>i :call SelectIndent()<CR>

" Sort current level of indentation
nmap <leader>s :call SelectIndent()<CR>:sort<CR>


" auto complete window config
set pumheight=6
let $FZF_DEFAULT_COMMAND = 'ag -g ""'
let g:ycm_autoclose_preview_window_after_insertion = 1
let g:ycm_autoclose_preview_window_after_completion = 1
set completeopt-=preview

" console log macro
let @j="Iconsole.log('xxx', xxx);\=\=f'wve"
let @k="Ixxx.subscribe(v => {console.log('xxx', v);});\=\=fxbve"
let @l="Itap(v => { console.log('xxx', v); }),\=\=fxve"
let @s=":Ag \<cr>"
let @i="I@Input() : xxx;\=\=f:i"
let @o="I@Output() = new EventEmitter<>();\=\=f=i"

" let g:loaded_python_provider=1

if $TERM_PROGRAM =~ "iTerm"
    let &t_SI = "\<Esc>]50;CursorShape=1\x7" " Vertical bar in insert mode
    let &t_EI = "\<Esc>]50;CursorShape=0\x7" " Block in normal mode
endif

"Disable error bells
set noerrorbells visualbell t_vb=
autocmd GUIEnter * set visualbell t_vb=

" tab cycle shortcut
nnoremap <leader>t :tabn<CR>
nnoremap <leader>T :tabe<CR>


" prettier
let g:prettier#config#bracket_spacing = 'true'
let g:prettier#config#single_quote = 'true'

" template highlighting in ts files
" autocmd FileType typescript JsPreTmpl html
" autocmd FileType typescript syn clear foldBraces

" closetag
let g:closetag_filenames = '*.ts,*.html,*.xhtml,*.phtml'

set mouse=a
nmap <leader>[ :so ~/.vimrc<CR>
nmap <leader>o :syntax sync minlines=20<CR>

nnoremap <silent> <leader>c :exec "color " .
    \ ((g:colors_name == "Carbonight") ? "solarized8_light_flat" : "Carbonight")<CR>

