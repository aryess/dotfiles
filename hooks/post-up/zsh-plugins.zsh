#!/bin/bash

CUSTOM_PLUGIN_PATH=$HOME/.config/oh-my-zsh/custom/plugins
if [[ ! -e $CUSTOM_PLUGIN_PATH ]]; then
    mkdir -p $CUSTOM_PLUGIN_PATH
fi

cd $CUSTOM_PLUGIN_PATH

#zsh-syntax-highlighting
if [[ ! -e $CUSTOM_PLUGIN_PATH/zsh-syntax-highlighting ]]; then
    echo "Installing zsh-syntax-highlighting"
    git clone https://github.com/zsh-users/zsh-syntax-highlighting.git --quiet
fi

if [[ -e $CUSTOM_PLUGIN_PATH/zsh-syntax-highlighting ]]; then
    echo "Updating zsh-syntax-highlighting"
    cd $CUSTOM_PLUGIN_PATH/zsh-syntax-highlighting
    git pull --quiet
fi