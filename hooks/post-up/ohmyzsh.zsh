#!/bin/bash

if [[ ! -e $HOME/.oh-my-zsh ]]; then
    git clone https://github.com/robbyrussell/oh-my-zsh.git $HOME/.oh-my-zsh
    cd $HOME/.oh-my-zsh
    git checkout `git describe --abbrev=0 --tags`
fi
