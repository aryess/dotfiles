for config in $HOME/.config/zsh/*.zsh; do
    if [[ ! -f $config ]]; then
        continue
    fi
    source $config
done
